package com.thuha.filemanagerusedb;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        loadFile();
        loadAudio();
    }

    private void loadAudio() {
        Uri dbPath = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] selectColums = new String[]{
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.COMPOSER,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TRACK,
                MediaStore.Audio.Media.DURATION
        };
        Cursor cursor = getContentResolver().query(dbPath, selectColums, null, null, null);
        if (cursor == null) {
            return;
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String id = getCData(cursor, MediaStore.Audio.Media._ID);
            String displayName = getCData(cursor, MediaStore.Audio.Media.DISPLAY_NAME);
            String title = getCData(cursor, MediaStore.Audio.Media.TITLE);
            String path = getCData(cursor,MediaStore.Audio.Media.DATA);
            // kich thuoc bang 0 la thu muc
            String size = getCData(cursor,MediaStore.Audio.Media.SIZE);
            String composer = getCData(cursor, MediaStore.Audio.Media.COMPOSER);
            String artist = getCData(cursor, MediaStore.Audio.Media.ARTIST);
            String track = getCData(cursor, MediaStore.Audio.Media.TRACK);
            String duration = getCData(cursor, MediaStore.Audio.Media.DURATION);
            String album = getCData(cursor,  MediaStore.Audio.Media.ALBUM);
            Log.i("TAG", "ID = " + id + "\nDisplayName = " + displayName +
                    "\nTitle = " + title + "\nComposer = " + composer + "\nPath = " + path);
            cursor.moveToNext();
        }
        cursor.close();
    }

    private void loadFile() {
        Uri dbPath = MediaStore.Files.getContentUri("external");
        String[] selectColums = new String[]{
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DISPLAY_NAME,
                MediaStore.Files.FileColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Files.FileColumns.RELATIVE_PATH,
                MediaStore.Files.FileColumns.PARENT,
                MediaStore.Files.FileColumns.MEDIA_TYPE
        };
        Cursor cursor = getContentResolver().query(dbPath, selectColums, null, null, null);
        if (cursor == null) {
            return;
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String id = getCData(cursor, MediaStore.Files.FileColumns._ID);
            String displayName = getCData(cursor, MediaStore.Files.FileColumns.DISPLAY_NAME);
            String title = getCData(cursor, MediaStore.Files.FileColumns.TITLE);
            String path = getCData(cursor,MediaStore.Files.FileColumns.DATA);
            // kich thuoc bang 0 la thu muc
            String size = getCData(cursor,MediaStore.Files.FileColumns.SIZE);
            String buketDisplayName = getCData(cursor, MediaStore.Files.FileColumns.BUCKET_DISPLAY_NAME);
            String relativePath = getCData(cursor, MediaStore.Files.FileColumns.RELATIVE_PATH);
            String parent = getCData(cursor, MediaStore.Files.FileColumns.PARENT);
            String mediaType = getCData(cursor, MediaStore.Files.FileColumns.MEDIA_TYPE);
            Log.i("TAG", "ID = " + id + "\nDisplayName = " + displayName +
                    "\nTitle = " + title + "\nBucketName = " + buketDisplayName + "\nPath = " + path +
                    "\nRelativePath = " + relativePath + "\nParent = " + parent + "\nMediaType = " + mediaType);
            cursor.moveToNext();
        }
        cursor.close();


    }

    private String getCData(Cursor c, String id) {
        return c.getString(c.getColumnIndex(id));
    }
}
